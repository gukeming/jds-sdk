### 一 web端使用方法

#### 1、引入sdk

<script src="https://xx.ds.cn/jds-sdk.js"></script>

#### 2、使用

##### 对sdk进行配置

| 参数    | 说明                                                         | 是否必填 | 默认值 |
| ------- | ------------------------------------------------------------ | -------- | ------ |
| debug   | 是否打印调用过程中日志                                       | 否       | false  |
| setting | 网页设置，在原生加载完后就会读取并回调到onPageSetting(String setting) | 否       | 无     |
|         |                                                              |          |        |
|         |                                                              |          |        |

例子

```javascript
jDsSdk.config({
	debug: true
});
```

```javascript

jDsSdk.config({
  debug: true,
  setting: {
    title: "大圣科技",
    fitsStatusBar: false,
    autoReolad: true,
    shareWeb: {
      url: 'https://www.baidu.com',
      title: "baidu",
      thumb: "https://www.baidu.com/img/pc_cc75653cd975aea6d4ba1f59b3697455.png",
      desc: "分享测试",
      channels: ['FRIEND', 'WEIXIN'],
      extra: {
        id: 1,
        xx: "123456"
      },
    },
  }
});
```



网页setting参数，可以根据自己需求定义（以下为参考）

| Setting 参数  | 说明                   | 类型    |
| ------------- | ---------------------- | ------- |
| title         | 网页标题               | string  |
| fitsStatusBar | 补充刘海屏             | boolean |
| autoReolad    | 其他页面回来后自动刷新 | boolean |
| shareWeb      | 分享信息，参考分享组件 | object  |



##### 方法调用

| 参数         | 说明                     | 是否必填 | 默认值 |
| ------------ | ------------------------ | -------- | ------ |
| pluginMethod | 插件方法(xplugin.xmethd) | 是       | 无     |
| params       | 调用参数                 | 否       | 无     |
| callback     | 回调方法                 | 否       | 无     |



##### 回调函数返回说明

| 参数    | 说明     | 类型   | 描述                                                         |
| ------- | -------- | ------ | ------------------------------------------------------------ |
| code    | 状态     | string | 0：成功，1: IO异常，2: 无插件 3:无方法 4:json异常 5: 其他异常 |
| message | 业务数据 |        | 数据类型根据具体业务定义                                     |
| msg     | 异常信息 | string | 成功返回ok                                                   |

例子

```javascript
jDsSdk.invoke('plugin.method', {
  age: 10086,
  name: 'gukeming'
}, function(result) {
  console.log("页面调用结果", JSON.stringify(result));
});
```

##### 获取app版本号

```javascript
jDsSdk.getAppVersion();
```

说明：通过在原生userAgent中的增加DSApp/1.0.0来传递app版本号，如果版本号不存在就不是在app内嵌的webview中


##### 获取App注入参数

```javascript
jDsSdk.getAppParam('token');
```

说明：必须在页面加载完成后才能获取

##### 获取单个url参数

```javascript
jDsSdk.getQueryParam('name');
```

##### 获取全部url全部参数

```javascript
jDsSdk.getQueryParams()
```

#####  请求拦截

```javascript
jDsSdk.setReqInterceptor(function(req) {
  console.log('请求拦截', JSON.stringify(req));
  return req;
});
```



##### 返回拦截

```javascript
jDsSdk.setResInterceptor(function(res, req) {
  console.log('返回拦截拦截', JSON.stringify(res), JSON.stringify(req));
  alert('拦截结果'  + JSON.stringify(res));
  return res;
});
```

说明： 如果返回拦截无返回值，那么将不会再继续回调到业务层



##### webview页面加载完成

```javascript
jDsSdk.onReady(function(){
  alert('jDsServiceReady');
})
```

##### android webview扩展

1、原生支持打电话

```html
<a href="tel:18819272037">打电话</a>
```

2、邮件支持

```html
<a href="mailto:gukeming@ds.cn">发送邮件</a>
```

3、文件和图片选择

```html
<p>
  <img class="img" width="100" height="100" id="previewimg">
</p>
<input class="select" type="file" accept="image/*" id="picfile" name="选择图片">

//图片选择
<script>
  document.querySelector('.select').addEventListener('change', function(e){
    var URL = window.URL || window.webkitURL;
    document.getElementById('previewimg').src = URL.createObjectURL(this.files[0]);
  });
</script>

说明：图片选择支持从拍照和相机选择，必须设置为image/*，如果不设置。将默认选择文件
```



##### 原生调用js方法并返回

在web中定义method方法

```javascript
jDsSdk.addNativeCallback("methodName", function(params)  {
  alert("原生调用js 参数" + params);
  return "js返回数据" + params;
});
```

JSCallbackContext.callJSMethod(String method, String params, final JsValueCallback jsValueCallback)

| 数              | 说明       | 是否必填 | 默认值 |
| --------------- | ---------- | -------- | ------ |
| method          | js名称     | 是       | 无     |
| params          | 调用js参数 | 否       | 无     |
| jsValueCallback | js调用返回 | 否       | 无     |

使用例子：

  ```java
callbackContext.callJSMethod("methodName");

callbackContext.callJSMethod("methodName", new JSCallbackContext.JsValueCallback() {
  @Override
  public void onValue(String value) {

  }
});

callbackContext.callJSMethod("methodName", "123", new JSCallbackContext.JsValueCallback() {
  @Override
  public void onValue(String value) {

  }
});

  ```

### 二 android业务插件开发

##### 日志打印

```
JSService.setLogLevel(JSLog.DEBUG);
```

##### 业务插件开发

``` java
public class DeviceInfoPlugin extends JSPlugin {
    private static final String TAG = "DeviceInfoPlugin";

    public static final String TEST_METHOD = "testMethod";
    
    @Override
    protected void onInitialize() {
        super.onInitialize();
    }

    @Override
    public boolean execute(String method, JSParams args, JSCallbackContext callbackContext) throws JSONException {
        Log.d("JSPlugin", "method==" + method);
        Log.d("JSPlugin", "args==" + args.jsonParamForkey("name"));

        if(TEST_METHOD.equals(method)) {
            JSONObject json = new JSONObject();
            json.put("method",method);
            json.put("name",args.jsonParamForkey("name"));

            //返回js调用处
            callbackContext.success(json);

            return true;
        }

        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
```



##### JSCallbackContext 使用说明



| 方法         | 说明                 | 是否必填 | 默认值 |
| ------------ | -------------------- | -------- | ------ |
| success      | 成功回调             |          |        |
| error        | 错误或失败回调       |          |        |
| callJSMethod | 原生调用js并返回结果 |          |        |



##### 插件注册

```
JSService.registerPlugin(String pluginName, Class<? extends JSPlugin> clazz);
```

| 参数       | 说明     | 是否必填 | 默认值 |
| ---------- | -------- | -------- | ------ |
| pluginName | 插件名称 | 是       | 无     |
| Class      | 插件类   | 是       | 无     |
|            |          |          |        |



##### 例子

``` Javens
JSService.registerPlugin("device", DeviceInfoPlugin.class);
```



### 三 iOS业务插件开发



