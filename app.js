const Koa = require('koa')
const app = new Koa()
const views = require('koa-views')
const json = require('koa-json')
const onerror = require('koa-onerror')
const bodyparser = require('koa-bodyparser')
const logger = require('koa-logger')
const cacheControl = require('koa-cache-control');
const conditional = require('koa-conditional-get');
const etag = require('koa-etag');


const index = require('./routes/index')
const users = require('./routes/users')

// error handler
onerror(app)

app.use(cacheControl({
  maxAge: 0
}));




app.use(function (ctx, next) {
  if (ctx.req)

  var url = ctx.request.url;
  //不是html就做强制缓存
  if(url.indexOf('.html') == -1) {
    ctx.cacheControl = {
        maxAge: 100000
    };
  }
  
  return next();
});

// middlewares
app.use(bodyparser({
  enableTypes:['json', 'form', 'text']
}))
app.use(json())
app.use(logger())

app.use(conditional());
// add etags
app.use(etag());

app.use(require('koa-static')(__dirname + '/public'))


app.use(views(__dirname + '/views', {
  extension: 'ejs'
}))




// logger
app.use(async (ctx, next) => {
  const start = new Date()
  await next()
  const ms = new Date() - start
  console.log(`${ctx.method} ${ctx.url} - ${ms}ms`)
})

// routes
app.use(index.routes(), index.allowedMethods())
app.use(users.routes(), users.allowedMethods())

// error-handling
app.on('error', (err, ctx) => {
  console.error('server error', err, ctx)
});

module.exports = app



